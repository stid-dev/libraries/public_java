package com.stid.libraries.docuware.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class DWLoginTokenRequest {
    @JsonProperty("Token")
    private String token;

    @JsonProperty(value = "HostID", defaultValue = "DocuWareSign-Service")
    private String hostId;

    @JsonProperty(value = "LicenseType", defaultValue = "PlatformService")
    private String licenseType;

    @JsonProperty("RememberMe")
    private boolean rememberMe;
}
