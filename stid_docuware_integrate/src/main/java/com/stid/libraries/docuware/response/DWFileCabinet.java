package com.stid.libraries.docuware.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class DWFileCabinet extends AbstractToJson {
    private String Id;
    private String Name;
    private String Color;
    private String VersionManagement;
    private boolean IsBasket;
    private boolean Usable;
    private boolean Default;
    private boolean WindowsExplorerClientAccess;
    private boolean AddIndexEntriesInUpperCase;
    private boolean DocumentAuditingEnabled;
    private boolean HasFullTextSupport;
}
