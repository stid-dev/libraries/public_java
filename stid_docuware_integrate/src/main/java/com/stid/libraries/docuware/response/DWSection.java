package com.stid.libraries.docuware.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class DWSection extends AbstractToJson {
    private String Id;
    private String ContentType;
    private long FileSize;
    private int PageCount;
    private String OriginalFileName;
}
