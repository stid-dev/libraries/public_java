package com.stid.libraries.docuware.provider;

import com.stid.libraries.docuware.request.DWLoginTokenRequest;
import com.stid.libraries.docuware.request.DWLoginUserPassRequest;
import com.stid.libraries.docuware.request.DWOrganizationTokenRequest;
import com.stid.libraries.docuware.request.DWUpdateIndexForm;
import com.stid.libraries.docuware.response.*;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface IDocuWareEndpoint {
    @FormUrlEncoded
    @POST(Endpoints.Account_Logon)
    Call<DWLoginResponse> loginUserPass(
            @Field("UserName") String userName,
            @Field("Password") String password,
            @Field("RememberMe") boolean rememberMe,
            @Field("RedirectToMyselfInCaseOfError") boolean redirectToMyselfInCaseOfError
    );

    @FormUrlEncoded
    @POST(Endpoints.Account_Logon)
    default Call<DWLoginResponse> loginUserPass(DWLoginUserPassRequest request) {
        return loginUserPass(request.getUserName(), request.getPassword(), request.isRememberMe(), request.isRedirectToMyselfInCaseOfError());
    }

    @FormUrlEncoded
    @POST(Endpoints.Account_TokenLogOn)
    Call<DWLoginResponse> loginToken(
            @Field("Token") String token,
            @Field("HostID") String hostId,
            @Field("LicenseType") String licenseType,
            @Field("RememberMe") boolean rememberMe);

    @FormUrlEncoded
    @POST(Endpoints.Account_TokenLogOn)
    default Call<DWLoginResponse> loginToken(@Field("data") DWLoginTokenRequest request) {
        return loginToken(request.getToken(), request.getHostId(), request.getLicenseType(), request.isRememberMe());
    }

    @POST(Endpoints.LoginToken)
    Call<ResponseBody> getLoginToken(@Body DWOrganizationTokenRequest request);

    @GET(Endpoints.Organizations)
    Call<DWOrganizations> getOrganizations();

    @GET(Endpoints.FileCabinets)
    Call<DWFileCabinets> getFileCabinets();

    @GET(Endpoints.FileCabinet_ById)
    Call<DWFileCabinet> getFileCabinetById(@Path("FileCabinetId") String fileCabinetId);

    @GET(Endpoints.Documents)
    Call<DWDocuments> getDocuments(@Path("FileCabinetId") String fileCabinetId);

    @GET(Endpoints.Document_ById)
    Call<DWDocument> getDocumentById(@Path("FileCabinetId") String fileCabinetId, @Path("DocumentId") String documentId);

    @PUT(Endpoints.Document_Fields)
    Call<ResponseBody> updateDocumentIndex(@Path("FileCabinetId") String fileCabinetId, @Path("DocumentId") String documentId, @Body DWUpdateIndexForm indexForm);

    @GET(Endpoints.Sections)
    Call<DWSections> getSections(@Path("FileCabinetId") String fileCabinetId, @Query("DocId") String documentId);

    @GET(Endpoints.Section_ById)
    Call<DWSection> getSectionById(@Path("FileCabinetId") String fileCabinetId, @Path("Section") String sectionId);

    @GET(Endpoints.Section_Data)
    Call<ResponseBody> downloadSection(@Path("FileCabinetId") String fileCabinetId, @Path("Section") String sectionId);

    @Multipart
    @POST(Endpoints.Sections)
    Call<DWSection> uploadSection(@Path("FileCabinetId") String fileCabinetId, @Query("DocId") String documentId, @Part MultipartBody.Part file);

    @DELETE(Endpoints.Section_ById)
    Call<ResponseBody> deleteSection(@Path("FileCabinetId") String fileCabinetId, @Path("Section") String sectionId);

    class Endpoints {
        public static final String Account_Logon = "/Docuware/Platform/Account/Logon";

        public static final String Account_TokenLogOn = "/Docuware/Platform/Account/TokenLogOn";
        public static final String LoginToken = "/Docuware/Platform/Organization/LoginToken";
        public static final String Organizations = "/Docuware/Platform/Organizations";
        public static final String FileCabinets = "/Docuware/Platform/FileCabinets";
        public static final String FileCabinet_ById = "/Docuware/Platform/FileCabinets/{FileCabinetId}";
        public static final String Documents = "/Docuware/Platform/FileCabinets/{FileCabinetId}/Documents";
        public static final String Document_ById = "/Docuware/Platform/FileCabinets/{FileCabinetId}/Documents/{DocumentId}";
        public static final String Document_Fields = "/Docuware/Platform/FileCabinets/{FileCabinetId}/Documents/{DocumentId}/Fields";
        public static final String Sections = "/Docuware/Platform/FileCabinets/{FileCabinetId}/Sections";
        public static final String Section_ById = "/Docuware/Platform/FileCabinets/{FileCabinetId}/Sections/{Section}";
        public static final String Section_Data = "/Docuware/Platform/FileCabinets/{FileCabinetId}/Sections/{Section}/Data";
    }
}
