package com.stid.libraries.docuware.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class DWOrganizationTokenRequest {
    private List<String> targetProducts;

    @JsonProperty(value = "Usage", defaultValue = "Multi")
    private String usage;

    @JsonProperty(value = "Lifetime", defaultValue = "1.00:00:00")
    private String lifetime;
}
