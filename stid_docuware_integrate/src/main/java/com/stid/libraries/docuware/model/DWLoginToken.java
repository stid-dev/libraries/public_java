package com.stid.libraries.docuware.model;

import com.stid.libraries.docuware.response.AbstractToJson;
import lombok.*;

import java.time.Instant;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class DWLoginToken extends AbstractToJson {
    private String token;
    private Instant expire;
}
