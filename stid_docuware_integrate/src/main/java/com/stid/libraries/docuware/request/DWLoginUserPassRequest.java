package com.stid.libraries.docuware.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class DWLoginUserPassRequest {
    @JsonProperty("UserName")
    private String userName;

    @JsonProperty("Password")
    private String password;

    @JsonProperty("RememberMe")
    private boolean rememberMe;

    @JsonProperty("RedirectToMyselfInCaseOfError")
    private boolean redirectToMyselfInCaseOfError;
}
