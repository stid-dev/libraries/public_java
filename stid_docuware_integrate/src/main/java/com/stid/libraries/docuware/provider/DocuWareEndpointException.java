package com.stid.libraries.docuware.provider;

public class DocuWareEndpointException extends Exception {
    public DocuWareEndpointException(String message) {
        super(message);
    }

    public DocuWareEndpointException(String message, Throwable cause) {
        super(message, cause);
    }
}
