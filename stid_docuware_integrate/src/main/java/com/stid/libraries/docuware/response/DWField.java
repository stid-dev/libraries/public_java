package com.stid.libraries.docuware.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Data
@ToString
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class DWField extends AbstractToJson {
    private boolean SystemField;
    private String FieldName;
    private String FieldLabel;
    private boolean IsNull;
    private boolean ReadOnly;
    private String Item;
    private String ItemElementName;
}
