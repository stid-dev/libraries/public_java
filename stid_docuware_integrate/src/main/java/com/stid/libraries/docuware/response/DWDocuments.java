package com.stid.libraries.docuware.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class DWDocuments extends AbstractToJson {
    private List<DWDocument> Items;
}
