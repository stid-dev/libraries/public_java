package com.stid.libraries.docuware.model;

import lombok.*;

@Getter
@Setter
@ToString(exclude = {"data"})
@AllArgsConstructor
@NoArgsConstructor
public class DWFile {
    private String name;
    private byte[] data;
    private Long size;
    private String contentType;

    public static DWFile create(String fileName, byte[] data, String contentType) {
        return new DWFile(fileName, data, data != null ? (long) data.length : 0, contentType);
    }
}

