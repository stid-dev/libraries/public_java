package com.stid.libraries.docuware.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
public class DWErrorResponse extends AbstractToJson {
    private String Message;
    private String Exception;
    private String Uri;
    private String Method;
    private String StatusCode;
    private String Status;
    private String InternalCode;
}
