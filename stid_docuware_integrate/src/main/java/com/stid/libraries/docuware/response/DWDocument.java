package com.stid.libraries.docuware.response;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@JsonNaming(value = PropertyNamingStrategies.UpperCamelCaseStrategy.class)
@JsonRootName(value = "Items")
public class DWDocument extends AbstractToJson {
    private String Id;
    private String FileCabinetId;
    private String Title;
    private int TotalPages;
    private String VersionStatus;
    private long FileSize;
    private int SectionCount;
    private String ContentType;
    private boolean HasXmlDigitalSignatures;
    private List<DWSection> Sections = new ArrayList<>();
    private List<DWField> Fields = new ArrayList<>();
}
