package com.stid.libraries.docuware.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.stid.libraries.docuware.util.GlobalUtil;

public abstract class AbstractToJson {
    public String toJson() {
        try {
            return GlobalUtil.OBJECT_MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "{}";
        }
    }
}
