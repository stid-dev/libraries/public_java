package com.stid.libraries.docuware.provider;

import com.stid.libraries.docuware.model.DWFile;
import com.stid.libraries.docuware.model.DWLoginToken;
import com.stid.libraries.docuware.request.DWLoginTokenRequest;
import com.stid.libraries.docuware.request.DWLoginUserPassRequest;
import com.stid.libraries.docuware.request.DWOrganizationTokenRequest;
import com.stid.libraries.docuware.request.DWUpdateIndexForm;
import com.stid.libraries.docuware.response.*;
import com.stid.libraries.docuware.util.GlobalUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public final class DocuWareEndpoint {
    private static final JacksonConverterFactory CONVERTER_FACTORY = JacksonConverterFactory.create(GlobalUtil.OBJECT_MAPPER);

    private final String host;
    private final String username;
    private final String password;

    private OkHttpClient httpClient;
    private IDocuWareEndpoint endpoint;

    @Getter
    @Setter
    private Duration tokenTimeout = Duration.ofDays(1);

    @Getter
    @Setter
    private int retryAuthorize = 3;

    @Getter
    @Setter
    private DWLoginToken dwLoginToken;

    public DocuWareEndpoint(String host, String username, String password) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.connect();
    }

    public void connect() {
        if (httpClient == null) {
            this.httpClient = new OkHttpClient.Builder()
                    .addInterceptor(chain -> {
                        Request originalRequest = chain.request();
                        Request modifiedRequest = originalRequest.newBuilder()
                                .header("Accept", "application/json")
                                .build();
                        return chain.proceed(modifiedRequest);
                    })
                    .cookieJar(new CookieJar() {
                        private final List<Cookie> cookies = new ArrayList<>();

                        @Override
                        public void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
                            list.stream().filter(cookie -> !cookie.value().isEmpty()).forEach(this.cookies::add);
                        }

                        @Override
                        public List<Cookie> loadForRequest(HttpUrl httpUrl) {
                            return cookies;
                        }
                    })
                    .build();
        }
        this.endpoint = new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(host)
                .addConverterFactory(CONVERTER_FACTORY)
                .build()
                .create(IDocuWareEndpoint.class);
    }

    public void shutdown() {
        httpClient.dispatcher().executorService().shutdown();
        httpClient = null;
        endpoint = null;
    }

    public void authorize() throws DocuWareEndpointException {
        authorize(retryAuthorize);
    }

    public List<DWOrganization> getOrganizations() throws DocuWareEndpointException {
        try {
            Response<DWOrganizations> response = endpoint.getOrganizations().execute();
            return handleResponse(response).getOrganization();
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public List<DWFileCabinet> getFileCabinets() throws DocuWareEndpointException {
        try {
            Response<DWFileCabinets> response = endpoint.getFileCabinets().execute();
            return handleResponse(response).getFileCabinet();
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public DWFileCabinet getFileCabinetById(String fileCabinetId) throws DocuWareEndpointException {
        try {
            Response<DWFileCabinet> response = endpoint.getFileCabinetById(fileCabinetId).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public List<DWDocument> getDocuments(String fileCabinetId) throws DocuWareEndpointException {
        try {
            Response<DWDocuments> response = endpoint.getDocuments(fileCabinetId).execute();
            return handleResponse(response).getItems();
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public DWDocument getDocumentsById(String fileCabinetId, String documentId) throws DocuWareEndpointException {
        try {
            Response<DWDocument> response = endpoint.getDocumentById(fileCabinetId, documentId).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public void updateDocumentIndex(String fileCabinetId, String documentId, DWUpdateIndexForm indexForm) throws DocuWareEndpointException {
        try {
            Response<ResponseBody> response = endpoint.updateDocumentIndex(fileCabinetId, documentId, indexForm).execute();
            handleResponseString(response);
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public List<DWSection> getSections(String fileCabinetId, String documentId) throws DocuWareEndpointException {
        try {
            Response<DWSections> response = endpoint.getSections(fileCabinetId, documentId).execute();
            return handleResponse(response).getSection();
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public DWSection getSectionById(String fileCabinetId, String sectionId) throws DocuWareEndpointException {
        try {
            Response<DWSection> response = endpoint.getSectionById(fileCabinetId, sectionId).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public byte[] downloadSectionAsBytes(String fileCabinetId, String sectionId) throws DocuWareEndpointException {
        try {
            Response<ResponseBody> response = endpoint.downloadSection(fileCabinetId, sectionId).execute();
            return handleResponseBytes(response);
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public DWFile downloadSectionAsFile(String fileCabinetId, String sectionId) throws DocuWareEndpointException {
        try {
            Response<DWSection> response = endpoint.getSectionById(fileCabinetId, sectionId).execute();
            DWSection dwSection = handleResponse(response);
            Response<ResponseBody> responseData = endpoint.downloadSection(fileCabinetId, sectionId).execute();
            byte[] sectionContent = handleResponseBytes(responseData);
            return DWFile.create(dwSection.getOriginalFileName(), sectionContent, dwSection.getContentType());
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public DWSection uploadSection(String fileCabinetId, String documentId, DWFile dwFile) throws DocuWareEndpointException {
        try {
            RequestBody requestBody = RequestBody.create(MediaType.parse(dwFile.getContentType()), dwFile.getData());
            MultipartBody.Part file = MultipartBody.Part.createFormData("file", dwFile.getName(), requestBody);
            Response<DWSection> response = endpoint.uploadSection(fileCabinetId, documentId, file).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    public void deleteSection(String fileCabinetId, String sectionId) throws DocuWareEndpointException {
        try {
            Response<ResponseBody> response = endpoint.deleteSection(fileCabinetId, sectionId).execute();
            handleResponseString(response);
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    // private methods
    private void authorize(int retry) throws DocuWareEndpointException {
        if (dwLoginToken == null || dwLoginToken.getToken() == null || dwLoginToken.getExpire().isBefore(Instant.now().plusSeconds(60))) {
            try {
                dwLoginToken = loginUserPass();
            } catch (DocuWareEndpointException e) {
                if (retry > 0) {
                    authorize(--retry);
                } else {
                    throw e;
                }
            }
        } else {
            try {
                dwLoginToken = loginToken();
            } catch (Exception e) {
                dwLoginToken = null;
                authorize(--retry);
            }
        }
    }

    private DWLoginToken loginUserPass() throws DocuWareEndpointException {
        log.info("LoginDocuWare...UserPass");
        try {
            DWLoginUserPassRequest request = DWLoginUserPassRequest.builder()
                    .userName(username).password(password)
                    .rememberMe(false).redirectToMyselfInCaseOfError(false)
                    .build();
            Response<DWLoginResponse> response = endpoint.loginUserPass(request).execute();
            DWLoginResponse responseBody = handleResponse(response);
            log.info("--Version={}", responseBody.getVersion());

            DWLoginToken loginToken = this.getLoginToken();
            log.info("--LoginToken: {}", loginToken);
            return loginToken;
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    private DWLoginToken loginToken() throws DocuWareEndpointException {
        log.info("LoginDocuWare...Token");
        try {
            DWLoginTokenRequest request = DWLoginTokenRequest.builder()
                    .hostId("DocuWareSign-Service").licenseType("PlatformService")
                    .token(dwLoginToken.getToken()).rememberMe(false)
                    .build();
            Response<DWLoginResponse> response = endpoint.loginToken(request).execute();
            DWLoginResponse responseBody = handleResponse(response);
            log.info("--Version={}", responseBody.getVersion());

            DWLoginToken loginToken = this.getLoginToken();
            log.info("--LoginToken: {}", loginToken);
            return loginToken;
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    private DWLoginToken getLoginToken() throws DocuWareEndpointException {
        try {
            Duration lifetime = tokenTimeout != null ? tokenTimeout : Duration.ofDays(1);
            Instant expire = Instant.now().plus(lifetime);
            DWOrganizationTokenRequest request = DWOrganizationTokenRequest.builder()
                    .targetProducts(Collections.singletonList("PlatformService"))
                    .lifetime(formatLifetime(lifetime))
                    .usage("Multi")
                    .build();
            Response<ResponseBody> response = endpoint.getLoginToken(request).execute();
            String token = handleResponseString(response);
            if (token == null || token.isBlank())
                throw new Exception("DocuWareEndpointException.LoginFailed");

            return new DWLoginToken(token, expire);
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    private <T> T handleResponse(Response<T> response) throws DocuWareEndpointException {
        T body = response.body();
        if (response.isSuccessful() && body != null) {
            log.info("ResponseBody: " + body);
            return body;
        } else {
            String errorMessage = response.message();
            ResponseBody errorBody = response.errorBody();
            if (errorBody != null) {
                try {
                    errorMessage = errorBody.string();
                    DWErrorResponse errorResponse = GlobalUtil.OBJECT_MAPPER.readValue(errorMessage, DWErrorResponse.class);
                    log.error("DWErrorResponse: {}", errorResponse.toJson());
                    errorMessage = errorResponse.getMessage();
                } catch (Exception ignored) {
                }
            }
            throw new DocuWareEndpointException(errorMessage);
        }
    }

    private byte[] handleResponseBytes(Response<ResponseBody> response) throws DocuWareEndpointException {
        try {
            if (response.isSuccessful()) {
                byte[] bytes = null;
                try (ResponseBody body = response.body()) {
                    if (body != null) {
                        bytes = body.bytes();
                    }
                }
                return bytes;
            } else {
                String errorMessage = response.message();
                try (ResponseBody errorBody = response.errorBody()) {
                    if (errorBody != null) {
                        errorMessage = errorBody.string();
                    }
                } catch (Exception e) {
                    errorMessage = e.getMessage();
                }
                throw new DocuWareEndpointException(errorMessage);
            }
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    private String handleResponseString(Response<ResponseBody> response) throws DocuWareEndpointException {
        try {
            if (response.isSuccessful()) {
                String string = null;
                try (ResponseBody body = response.body()) {
                    if (body != null) {
                        string = body.string();
                    }
                }
                return string;
            } else {
                String errorMessage = response.message();
                try (ResponseBody errorBody = response.errorBody()) {
                    if (errorBody != null) {
                        errorMessage = errorBody.string();
                    }
                } catch (Exception e) {
                    errorMessage = e.getMessage();
                }
                throw new DocuWareEndpointException(errorMessage);
            }
        } catch (Exception e) {
            throw new DocuWareEndpointException(e.getMessage());
        }
    }

    private String formatLifetime(Duration duration) {
        long days = duration.toDays();
        long hours = duration.toHours() % 24;
        long minutes = duration.toMinutes() % 60;
        long seconds = duration.getSeconds() % 60;

        return String.format("%d.%02d:%02d:%02d", days, hours, minutes, seconds);
    }
}
