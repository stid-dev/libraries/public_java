package com.stid.libraries.fido2server.endpoint.constant;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum UserVerification {
    REQUIRED("required"),
    PREFERRED("preferred"),
    DISCOURAGED("discouraged");

    @JsonValue
    private final String value;

    UserVerification(String value) {
        this.value = value;
    }
}
