package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.stid.libraries.fido2server.endpoint.util.ResponseBase;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
public class AttestationResult extends ResponseBase {
    private UUID id;
    private String name;
    private String format;
    private String credentialId;
    private String aaguid;
    @JsonAlias({"createDate", "createTime", "createdDate", "createdTime"})
    private Instant createdDate;
    @JsonAlias({"lastAccess", "lastAccessDate", "lastAccessTime"})
    private Instant lastAccess;
    @JsonAlias({"counter", "signatureCounter"})
    private long counter;
    private String username;
    private Set<String> transports;
}
