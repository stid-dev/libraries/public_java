package com.stid.libraries.fido2server.endpoint.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Builder
public class UpdateInfoRequest {
    private Set<String> subdomains;
    private Set<Integer> ports;
}
