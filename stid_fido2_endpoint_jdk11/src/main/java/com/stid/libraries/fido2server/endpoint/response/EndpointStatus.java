package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.stid.libraries.fido2server.endpoint.util.Status;
import lombok.*;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class EndpointStatus {
    private Status status;
    private String errorMessage;
}
