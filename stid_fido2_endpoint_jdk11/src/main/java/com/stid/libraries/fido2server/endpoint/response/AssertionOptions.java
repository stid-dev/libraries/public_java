package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.stid.libraries.fido2server.endpoint.util.JsonObject;
import com.stid.libraries.fido2server.endpoint.util.ResponseBase;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssertionOptions extends ResponseBase {
    private String challenge;
    private long timeout;
    private String rpId;
    private List<JsonObject> allowCredentials;
    private String userVerification;
    private JsonObject extensions;
}
