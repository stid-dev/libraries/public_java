package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LogEvent {
    private UUID id;
    private EventName eventName;
    private EventType eventType;
    private EventStatus eventStatus;
    private String eventDetail;
    private Instant timestamp;

    public enum EventName {
        RELYING_PARTY_GET_ACCESS_TOKEN, RELYING_PARTY_UPDATE_INFO, RELYING_PARTY_CREATE_USER, RELYING_PARTY_DELETE_USER, RELYING_PARTY_UPDATE_AUTHENTICATOR, RELYING_PARTY_DELETE_AUTHENTICATOR, RELYING_PARTY_ATTESTATION_OPTIONS, RELYING_PARTY_ATTESTATION_RESULT, RELYING_PARTY_ASSERTION_OPTIONS, RELYING_PARTY_ASSERTION_RESULT
    }

    public enum EventType {
        CREATE, UPDATE, DELETE, AUTHENTICATION;
    }

    public enum EventStatus {
        SUCCESS, FAILURE;
    }
}
