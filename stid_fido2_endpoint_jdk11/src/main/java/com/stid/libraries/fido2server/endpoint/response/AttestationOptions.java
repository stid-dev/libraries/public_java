package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.stid.libraries.fido2server.endpoint.util.JsonObject;
import com.stid.libraries.fido2server.endpoint.util.ResponseBase;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AttestationOptions extends ResponseBase {
    private RP rp;
    private User user;
    private String challenge;
    private List<JsonObject> pubKeyCredParams;
    private long timeout;
    private List<JsonObject> excludeCredentials;
    private JsonObject authenticatorSelection;
    private String attestation;
    private JsonObject extensions;

    @Data
    public static class RP {
        private String id;
        private String name;
    }

    @Data
    public static class User {
        private String id;
        private String name;
        private String displayName;
    }
}
