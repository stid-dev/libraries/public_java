package com.stid.libraries.fido2server.endpoint.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import lombok.Getter;

@Getter
public enum Status {

    OK("ok"),
    FAILED("failed");

    @JsonValue
    private final String value;

    Status(String value) {
        this.value = value;
    }

    public static Status create(String value) {
        switch (value) {
            case "ok":
                return OK;
            case "failed":
                return FAILED;
            default:
                throw new IllegalArgumentException();
        }
    }

    @JsonCreator
    public static Status fromJson(String value) throws InvalidFormatException {
        try {
            return create(value);
        } catch (IllegalArgumentException e) {
            throw new InvalidFormatException(null, "value is out of range", value, Status.class);
        }
    }

}
