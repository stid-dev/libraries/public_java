package com.stid.libraries.fido2server.endpoint.request;

import com.stid.libraries.fido2server.endpoint.constant.UserVerification;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AssertionOptionsRequest {
    private String username;
    private UserVerification userVerification;
}
