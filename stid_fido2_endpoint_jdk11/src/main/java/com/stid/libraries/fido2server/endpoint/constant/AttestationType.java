package com.stid.libraries.fido2server.endpoint.constant;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum AttestationType {
    NONE("none"),
    INDIRECT("indirect"),
    DIRECT("direct"),
    ENTERPRISE("enterprise");

    @JsonValue
    private final String value;

    AttestationType(String value) {
        this.value = value;
    }
}
