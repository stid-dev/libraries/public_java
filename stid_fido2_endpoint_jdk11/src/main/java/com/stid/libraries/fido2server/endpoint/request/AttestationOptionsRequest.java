package com.stid.libraries.fido2server.endpoint.request;

import com.stid.libraries.fido2server.endpoint.constant.AttestationType;
import com.stid.libraries.fido2server.endpoint.constant.AuthenticatorAttachment;
import com.stid.libraries.fido2server.endpoint.constant.UserVerification;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AttestationOptionsRequest {
    private String username;
    private String displayName;
    private AttestationType attestation;
    private AuthenticatorSelection authenticatorSelection;

    @Getter
    @Setter
    @Builder
    public static class AuthenticatorSelection {
        private AuthenticatorAttachment authenticatorAttachment;
        private UserVerification userVerification;
        private boolean requireResidentKey;
    }
}
