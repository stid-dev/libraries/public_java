package com.stid.libraries.fido2server.endpoint.provider;

public class Fido2ServerEndpointException extends Exception {
    public Fido2ServerEndpointException(String message) {
        super(message);
    }

    public Fido2ServerEndpointException(String message, Throwable cause) {
        super(message, cause);
    }
}
