package com.stid.libraries.fido2server.endpoint.provider;

import com.stid.libraries.fido2server.endpoint.request.*;
import com.stid.libraries.fido2server.endpoint.response.*;
import com.stid.libraries.fido2server.endpoint.util.GlobalUtil;
import com.stid.libraries.fido2server.endpoint.util.JsonObject;
import lombok.Getter;
import lombok.Setter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public final class Fido2ServerEndpoint {
    private final String host;
    private final String clientId;
    private final String clientSecret;

    @Getter
    @Setter
    private long tokenTimeoutInSeconds = 600;

    private IFido2ServerEndpoint endpoint;
    private AccessToken accessToken;

    public Fido2ServerEndpoint(String host, String clientId, String clientSecret) throws Fido2ServerEndpointException {
        this.host = host;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.connect();
    }

    public AccessToken getAccessToken(boolean requireNew) throws Fido2ServerEndpointException {
        if (requireNew || accessToken == null || accessToken.getExpiresAt().isBefore(Instant.now())) {
            accessToken = getAccessToken();
        }
        return accessToken;
    }

    public AccessToken getAccessToken() throws Fido2ServerEndpointException {
        try {
            AccessTokenRequest request = AccessTokenRequest.builder()
                    .clientId(clientId)
                    .clientSecret(clientSecret)
                    .timeoutInSeconds(tokenTimeoutInSeconds)
                    .build();
            Response<AccessToken> response = endpoint.getAccessToken(request).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public EndpointStatus getStatus() throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<EndpointStatus> response = endpoint.getStatus().execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public ServiceLicense getLicense() throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<ServiceLicense> response = endpoint.getLicense().execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public ServiceInfo getInfo() throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<ServiceInfo> response = endpoint.getInfo().execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public ServiceInfo updateInfo(UpdateInfoRequest request) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<ServiceInfo> response = endpoint.updateInfo(request).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public List<User> getUsers() throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<List<User>> response = endpoint.getUsers().execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public User createUser(CreateUserRequest request) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<User> response = endpoint.createUser(request).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public User getUserById(UUID id) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<User> response = endpoint.getUserById(id).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public User deleteUserById(UUID id) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<User> response = endpoint.deleteUserById(id).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public List<Authenticator> getAuthenticators(UUID userId) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<List<Authenticator>> response = endpoint.getAuthenticators(userId).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public Authenticator updateAuthenticator(UUID userId,
                                             UUID authenticatorId,
                                             UpdateAuthenticatorRequest request) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<Authenticator> response = endpoint.updateAuthenticator(userId, authenticatorId, request).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public Authenticator deleteAuthenticator(UUID userId, UUID authenticatorId) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<Authenticator> response = endpoint.deleteAuthenticator(userId, authenticatorId).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public List<LogEvent> getLogEvents(Instant start, Instant end) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<List<LogEvent>> response = endpoint.getLogEvents(start, end).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public List<LogEvent> getLogEventsByType(LogEvent.EventName eventName,
                                             Instant start,
                                             Instant end) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<List<LogEvent>> response = endpoint.getLogEventsByName(eventName, start, end).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public List<LogEvent> getLogEventsByType(LogEvent.EventType eventType,
                                             Instant start,
                                             Instant end) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<List<LogEvent>> response = endpoint.getLogEventsByType(eventType, start, end).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public List<LogEvent> getLogEventsByUser(UUID userId,
                                             Instant start,
                                             Instant end) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<List<LogEvent>> response = endpoint.getLogEventsByUser(userId, start, end).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public AttestationOptions attestationOption(AttestationOptionsRequest options) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<AttestationOptions> response = endpoint.attestationOption(options).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public AttestationResult attestationResult(JsonObject attestationResult) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<AttestationResult> response = endpoint.attestationResult(attestationResult).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public AssertionOptions assertionOptions(AssertionOptionsRequest options) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            Response<AssertionOptions> response = endpoint.assertionOptions(options).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public AssertionResult assertionResult(JsonObject assertionResult) throws Fido2ServerEndpointException {
        try {
            getAccessToken(false);
            final Response<AssertionResult> response = endpoint.assertionResult(assertionResult).execute();
            return handleResponse(response);
        } catch (Exception e) {
            throw new Fido2ServerEndpointException(e.getMessage());
        }
    }

    public void connect() throws Fido2ServerEndpointException {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    if (accessToken != null) {
                        Request originalRequest = chain.request();
                        Request modifiedRequest = originalRequest.newBuilder()
                                .header("Authorization", accessToken.getAuthorization())
                                .build();
                        return chain.proceed(modifiedRequest);
                    }
                    return chain.proceed(chain.request());
                })
                .build();

        this.endpoint = new Retrofit.Builder()
                .client(httpClient)
                .baseUrl(host)
                .addConverterFactory(JacksonConverterFactory.create(GlobalUtil.OBJECT_MAPPER))
                .build()
                .create(IFido2ServerEndpoint.class);
        this.accessToken = getAccessToken(true);
    }

    private <T> T handleResponse(Response<T> response) throws Fido2ServerEndpointException {
        if (response.isSuccessful() && response.body() != null) {
            return response.body();
        } else {
            String errorMessage = response.message();
            if (response.errorBody() != null) {
                try {
                    EndpointStatus endpointStatus = GlobalUtil.OBJECT_MAPPER.readValue(response.errorBody().byteStream(), EndpointStatus.class);
                    errorMessage = endpointStatus.getErrorMessage();
                } catch (Exception ignored) {
                }
            }
            throw new Fido2ServerEndpointException(errorMessage);
        }
    }

}
