package com.stid.libraries.fido2server.endpoint.provider;

import com.stid.libraries.fido2server.endpoint.request.AssertionOptionsRequest;
import com.stid.libraries.fido2server.endpoint.request.AttestationOptionsRequest;
import com.stid.libraries.fido2server.endpoint.request.*;
import com.stid.libraries.fido2server.endpoint.response.*;
import com.stid.libraries.fido2server.endpoint.util.JsonObject;
import retrofit2.Call;
import retrofit2.http.*;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public interface IFido2ServerEndpoint {
    @POST(Endpoints.ACCESS_TOKEN)
    Call<AccessToken> getAccessToken(@Body AccessTokenRequest request);

    @GET(Endpoints.STATUS)
    Call<EndpointStatus> getStatus();

    @GET(Endpoints.LICENSE)
    Call<ServiceLicense> getLicense();

    @GET(Endpoints.INFO)
    Call<ServiceInfo> getInfo();

    @PUT(Endpoints.INFO)
    Call<ServiceInfo> updateInfo(@Body UpdateInfoRequest request);

    @GET(Endpoints.USER)
    Call<List<User>> getUsers();

    @POST(Endpoints.USER)
    Call<User> createUser(@Body CreateUserRequest request);

    @GET(Endpoints.USER_ID)
    Call<User> getUserById(@Path("id") UUID id);

    @DELETE(Endpoints.USER_ID)
    Call<User> deleteUserById(@Path("id") UUID id);

    @GET(Endpoints.AUTHENTICATOR)
    Call<List<Authenticator>> getAuthenticators(@Path("id") UUID userId);

    @PUT(Endpoints.AUTHENTICATOR_ID)
    Call<Authenticator> updateAuthenticator(@Path("id") UUID userId,
                                            @Path("authenticatorId") UUID authenticatorId,
                                            @Body UpdateAuthenticatorRequest request);

    @DELETE(Endpoints.AUTHENTICATOR_ID)
    Call<Authenticator> deleteAuthenticator(@Path("id") UUID userId,
                                            @Path("authenticatorId") UUID authenticatorId);

    @GET(Endpoints.EVENT)
    Call<List<LogEvent>> getLogEvents(@Query("start") Instant start,
                                      @Query("end") Instant end);

    @GET(Endpoints.EVENT_NAME)
    Call<List<LogEvent>> getLogEventsByName(@Path("eventName") LogEvent.EventName eventName,
                                            @Query("start") Instant start,
                                            @Query("end") Instant end);

    @GET(Endpoints.EVENT_TYPE)
    Call<List<LogEvent>> getLogEventsByType(@Path("eventType") LogEvent.EventType eventType,
                                            @Query("start") Instant start,
                                            @Query("end") Instant end);

    @GET(Endpoints.EVENT_USER)
    Call<List<LogEvent>> getLogEventsByUser(@Path("userId") UUID userId,
                                            @Query("start") Instant start,
                                            @Query("end") Instant end);

    @POST(Endpoints.ATTESTATION_OPTIONS)
    Call<AttestationOptions> attestationOption(@Body AttestationOptionsRequest options);

    @POST(Endpoints.ATTESTATION_RESULT)
    Call<AttestationResult> attestationResult(@Body JsonObject attestationResult);

    @POST(Endpoints.ASSERTION_OPTIONS)
    Call<AssertionOptions> assertionOptions(@Body AssertionOptionsRequest options);

    @POST(Endpoints.ASSERTION_RESULT)
    Call<AssertionResult> assertionResult(@Body JsonObject assertionResult);

    class Endpoints {
        public static final String ACCESS_TOKEN = "/api/auth/endpoint/access-token";

        public static final String STATUS = "/api/endpoint/status";
        public static final String INFO = "/api/endpoint/info";
        public static final String LICENSE = "/api/endpoint/license";

        public static final String USER = "/api/endpoint/users";
        public static final String USER_ID = "/api/endpoint/users/{id}";

        public static final String AUTHENTICATOR = "/api/endpoint/users/{id}/authenticators";
        public static final String AUTHENTICATOR_ID = "/api/endpoint/users/{id}/authenticators/{authenticatorId}";

        public static final String EVENT = "/api/endpoint/events";
        public static final String EVENT_NAME = "/api/endpoint/events/name/{eventName}";
        public static final String EVENT_TYPE = "/api/endpoint/events/type/{eventType}";
        public static final String EVENT_USER = "/api/endpoint/events/user/{userId}";

        public static final String ATTESTATION_OPTIONS = "/api/endpoint/attestation/options";
        public static final String ATTESTATION_RESULT = "/api/endpoint/attestation/result";

        public static final String ASSERTION_OPTIONS = "/api/endpoint/assertion/options";
        public static final String ASSERTION_RESULT = "/api/endpoint/assertion/result";
    }
}
