package com.stid.libraries.fido2server.endpoint.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseBase {
    private final Status status;
    private final String errorMessage;

    public ResponseBase(Status status, String errorMessage) {
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public ResponseBase() {
        this.status = Status.OK;
        this.errorMessage = "";
    }

    public String toJson() {
        try {
            return GlobalUtil.OBJECT_MAPPER.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
