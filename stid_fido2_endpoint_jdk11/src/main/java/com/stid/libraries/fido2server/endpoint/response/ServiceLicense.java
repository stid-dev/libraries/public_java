package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceLicense {
    private Quantity user;
    private Quantity subdomain;
    private Quantity port;
    private Duration time;
    private List<Package> packages;

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Quantity {
        private long totalQuantity;
        private long usedQuantity;
        private long freeQuantity;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Duration {
        private Instant expiration;
        private long remainingSeconds;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Package {
        private UUID id;
        private PackageType type;
        private long amount;
        private Instant createdDate;
        private Instant activatedDate;
        private String description;

        public enum PackageType {
            TIME,
            USER,
            SUBDOMAIN,
            PORT
        }
    }
}
