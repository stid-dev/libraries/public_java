package com.stid.libraries.fido2server.endpoint.constant;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum AuthenticatorAttachment {
    NONE(""),
    PLATFORM("platform"),
    CROSS_PLATFORM("cross-platform");

    @JsonValue
    private final String value;

    AuthenticatorAttachment(String value) {
        this.value = value;
    }
}
