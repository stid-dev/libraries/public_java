package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.Set;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Authenticator {
    private String id;
    private String name;
    private String credentialId;
    private String aaguid;
    private String coseKey;
    private String format;
    private Set<String> transports;
    private Instant createdDate;
    private Instant lastAccess;
    private long counter;
}
