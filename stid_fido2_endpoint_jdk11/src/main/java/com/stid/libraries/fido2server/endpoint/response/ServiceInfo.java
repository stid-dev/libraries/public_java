package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceInfo {
    private UUID id;
    private String secret;
    private String name;
    private String origin;
    private Set<String> subdomains;
    private Set<Integer> ports;
    private Set<String> origins;
    private String description;
    private Instant createdDate;
    private Status status;
    private Contact contact;

    public enum Status {
        INACTIVE, ACTIVE
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Contact {
        private String name;
        private String email;
        private String address;
        private String phone;
        private String description;
        private String updatedDate;
    }
}
