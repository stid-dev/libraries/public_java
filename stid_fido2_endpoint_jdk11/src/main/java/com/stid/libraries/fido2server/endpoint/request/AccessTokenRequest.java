package com.stid.libraries.fido2server.endpoint.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AccessTokenRequest {
    private String clientId;
    private String clientSecret;
    private Long timeoutInSeconds;
}
