package com.stid.libraries.fido2server.endpoint.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccessToken {
    private String id;
    private String subject;
    private String token;
    private Instant expiresAt;
    private String type = "Bearer";

    public String getAuthorization() {
        return type + " " + token;
    }
}
