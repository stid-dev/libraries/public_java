package com.stid.module.core.convert;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(exclude = "image")
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class RemoveBackgroundResult {
    private byte[] image;
    private boolean success;
    private String message;

    protected static RemoveBackgroundResult error(String message) {
        return new RemoveBackgroundResult(null, false, message);
    }

    protected static RemoveBackgroundResult success(byte[] image) {
        if (image == null)
            return error("Image content is null");
        if (image.length == 0)
            return error("Image content is empty");
        return new RemoveBackgroundResult(image, true, null);
    }
}

