package com.stid.module.core.certificate;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ocsp.OCSPObjectIdentifiers;
import org.bouncycastle.asn1.ocsp.OCSPResponseStatus;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.ocsp.*;
import org.bouncycastle.operator.DigestCalculator;
import org.bouncycastle.operator.bc.BcDigestCalculatorProvider;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

public class OCSPVerifier {
    public List<String> getOcsp(X509Certificate x509Certificate) throws Exception {
        List<String> list = new ArrayList<>();
        X509CertificateHolder holder;
        try {
            holder = new X509CertificateHolder(x509Certificate.getEncoded());
        } catch (Exception e) {
            throw new Exception("Cannot read certificate to get OSCP urls", e);
        }

        final AuthorityInformationAccess informationAccess = AuthorityInformationAccess.fromExtensions(holder.getExtensions());
        if (informationAccess == null) {
            throw new Exception("Certificate doesn't have Authority Information Access");
        }
        for (AccessDescription accessDescription : informationAccess.getAccessDescriptions()) {
            final ASN1ObjectIdentifier accessMethod = accessDescription.getAccessMethod();
            final GeneralName accessLocation = accessDescription.getAccessLocation();
            if (AccessDescription.id_ad_ocsp.equals(accessMethod)) {
                String ocspUrl = DERIA5String.fromByteArray(accessLocation.getName().toASN1Primitive().getEncoded()).toString();
                list.add(ocspUrl);
            }
        }
        return list;
    }

    public X509Certificate downloadIssuerCert(X509Certificate peerCert) throws Exception {
        final byte[] extensionValue = peerCert.getExtensionValue(Extension.authorityInfoAccess.getId());
        final AuthorityInformationAccess informationAccess = AuthorityInformationAccess.getInstance(JcaX509ExtensionUtils.parseExtensionValue(extensionValue));

        if (informationAccess != null) {
            for (AccessDescription accessDescription : informationAccess.getAccessDescriptions()) {
                final ASN1ObjectIdentifier accessMethod = accessDescription.getAccessMethod();
                final GeneralName accessLocation = accessDescription.getAccessLocation();
                if (AccessDescription.id_ad_caIssuers.equals(accessMethod)) {
                    String issuerCA = DERIA5String.fromByteArray(accessLocation.getName().toASN1Primitive().getEncoded()).toString();
                    URL url = new URI(issuerCA).toURL();
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");
                    con.connect();

                    final int status = con.getResponseCode();
                    if (status == HttpURLConnection.HTTP_OK) {
                        return CertificateLoader.loadX509Certificate(con.getInputStream().readAllBytes()).orElse(null);
                    } else {
                        throw new Exception("HTTP status code is not 200. code:" + status);
                    }
                }
            }
        }
        throw new Exception("Certification Authority Issuer was not exist.");
    }

    public RevocationStatus checkStatus(X509Certificate peerCert) throws Exception {
        X509Certificate issuerCert = downloadIssuerCert(peerCert);
        OCSPReq request = generateRequest(issuerCert, peerCert.getSerialNumber());
        //This list will sometimes have non ocsp urls as well.
        List<String> ocspUrls = getOcsp(peerCert);

        for (String ocspUrl : ocspUrls) {
            SingleResp[] responses;
            try {
                OCSPResp ocspResponse = getResponse(ocspUrl, request);
                if (OCSPResponseStatus.SUCCESSFUL != ocspResponse.getStatus()) {
                    System.out.println(ocspResponse);
                    continue; // Server didn't give the response right.
                }
                BasicOCSPResp basicResponse = (BasicOCSPResp) ocspResponse.getResponseObject();
                responses = (basicResponse == null) ? null : basicResponse.getResponses();
            } catch (OCSPException e) {
                continue;
            }

            if (responses != null && responses.length == 1) {
                SingleResp resp = responses[0];
                return getStatus(resp);
            }
        }
        throw new Exception("Can't get Revocation Status from OCSP.");
    }

    private OCSPReq generateRequest(X509Certificate issuerCert, BigInteger serialNumber) throws Exception {
        try {
            DigestCalculator digestCalculator = new BcDigestCalculatorProvider().get(CertificateID.HASH_SHA1);
            X509CertificateHolder x509CertificateHolder = new X509CertificateHolder(issuerCert.getEncoded());
            CertificateID certificateID = new CertificateID(digestCalculator, x509CertificateHolder, serialNumber);

            BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());
            Extension extension = new Extension(OCSPObjectIdentifiers.id_pkix_ocsp_nonce, false, new DEROctetString(nonce.toByteArray()));

            return new OCSPReqBuilder()
                    .addRequest(certificateID)
                    .setRequestExtensions(new Extensions(extension))
                    .build();
        } catch (Exception e) {
            throw new Exception("Cannot generate OSCP Request with the given certificate", e);
        }
    }

    private OCSPResp getResponse(String serviceUrl, OCSPReq request) throws Exception {
        //Todo: Use http client.
        byte[] array = request.getEncoded();
        if (serviceUrl.startsWith("http")) {
            try {
                HttpURLConnection con;
                URL url = new URL(serviceUrl);
                con = (HttpURLConnection) url.openConnection();
                con.setRequestProperty("Content-Type", "application/ocsp-request");
                con.setRequestProperty("Accept", "application/ocsp-response");
                con.setDoOutput(true);
                OutputStream out = con.getOutputStream();
                DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));
                dataOut.write(array);

                dataOut.flush();
                dataOut.close();

                //Check errors in response:
                if (con.getResponseCode() / 100 != 2) {
                    throw new Exception("Error getting ocsp response.Response code is " + con.getResponseCode());
                }

                //Get Response
                InputStream in = (InputStream) con.getContent();
                return new OCSPResp(in);
            } catch (Exception e) {
                throw new Exception("Cannot get ocspResponse from url: " + serviceUrl, e);
            }
        } else {
            throw new Exception("Only http is supported for ocsp calls");
        }
    }

    private RevocationStatus getStatus(SingleResp resp) throws Exception {
        Object status = resp.getCertStatus();
        if (status == CertificateStatus.GOOD) {
            return RevocationStatus.GOOD;
        } else if (status instanceof RevokedStatus) {
            return RevocationStatus.REVOKED;
        } else if (status instanceof UnknownStatus) {
            return RevocationStatus.UNKNOWN;
        }
        throw new Exception("Cant recognize the Certificate Status");
    }

}
