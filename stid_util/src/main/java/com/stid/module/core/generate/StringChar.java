package com.stid.module.core.generate;

/**
 * Utility class for generating random Strings.
 */
public enum StringChar {
    AlphaUpper("ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
    AlphaLower(AlphaUpper.getValue().toLowerCase()),
    Numeric("0123456789"),
    AlphaNumeric(AlphaUpper.getValue() + AlphaLower.getValue() + Numeric.getValue());

    private String value;

    StringChar(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
