package com.stid.module.core.certificate;

import com.stid.module.core.digest.DigestUtils;
import com.stid.module.core.digest.HashAlgorithm;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Optional;

public final class CertificateLoader {
    private static final Logger LOGGER = LoggerFactory.getLogger(CertificateLoader.class);

    public static Optional<X509Certificate> loadX509Certificate(byte[] bytes, boolean checkValidity) {
        try {
            X509Certificate certificate = (X509Certificate) CertificateFactory.getInstance("x509")
                    .generateCertificate(new ByteArrayInputStream(bytes));
            if (checkValidity)
                certificate.checkValidity();
            return Optional.ofNullable(certificate);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public static Optional<X509Certificate> loadX509Certificate(byte[] bytes) {
        return loadX509Certificate(bytes, false);
    }

    public static Optional<KeyStore> loadKeyStore(byte[] bytes, char[] password) {
        try {
            if (bytes != null && bytes.length > 0) {
                KeyStore keystore = KeyStore.getInstance("pkcs12");
                keystore.load(new ByteArrayInputStream(bytes), password);
                return Optional.of(keystore);
            }
        } catch (Exception e) {
            LOGGER.error("loadKeyStore failed: {}", e.getMessage());
        }
        return Optional.empty();
    }

    public static String getCertHashHexString(X509Certificate x509Certificate) {
        byte[] bytes = getCertHash(x509Certificate);
        return bytes != null ? Hex.toHexString(bytes) : null;
    }

    public static byte[] getCertHash(X509Certificate x509Certificate) {
        try {
            return DigestUtils.generateHash(HashAlgorithm.SHA1, x509Certificate.getEncoded());
        } catch (Exception e) {
            return null;
        }
    }
}
