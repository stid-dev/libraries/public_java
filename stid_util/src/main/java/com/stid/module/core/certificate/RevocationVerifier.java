package com.stid.module.core.certificate;

import java.security.cert.X509Certificate;

public final class RevocationVerifier {
   private static final CRLVerifier crlVerifier = new CRLVerifier(true);
   private static final OCSPVerifier ocspVerifier = new OCSPVerifier();
    public static RevocationResult verifyCrl(X509Certificate x509Certificate) {
        try {
            final RevocationStatus status = crlVerifier.checkStatus(x509Certificate);
            return new RevocationResult(status);
        } catch (Exception e) {
            return new RevocationResult(RevocationStatus.UNKNOWN, e.getMessage());
        }
    }

    public static RevocationResult verifyOcsp(X509Certificate x509Certificate) {
        try {
            final RevocationStatus status = ocspVerifier.checkStatus(x509Certificate);
            return new RevocationResult(status);
        } catch (Exception e) {
            return new RevocationResult(RevocationStatus.UNKNOWN, e.getMessage());
        }
    }

}
