package com.stid.module.core.convert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

public interface IRemoveBackgroundService {
    Logger LOGGER = LoggerFactory.getLogger(IRemoveBackgroundService.class);
    int getFreeCall();

    RemoveBackgroundResult removeBackground(byte[] file);
    RemoveBackgroundResult removeBackground(String base64);
    RemoveBackgroundResult removeBackground(URI url);
}
