package com.stid.module.core.generate;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Utility class for generating random Strings.
 */
public final class RandomUtils {
    private static final SecureRandom SECURE_RANDOM;

    static {
        SECURE_RANDOM = new SecureRandom();
        SECURE_RANDOM.nextBytes(new byte[64]);
    }

    public static byte[] randomBytes(int length) {
        byte[] bytes = new byte[length];
        SECURE_RANDOM.nextBytes(bytes);
        return bytes;
    }

    public static String randomString(int length, String stringChars) {
        if (stringChars == null || stringChars.isBlank())
            stringChars = StringChar.AlphaNumeric.getValue();
        StringBuilder randomString = new StringBuilder(stringChars);

        Random random = new Random();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(randomString.length());
            result.append(randomString.charAt(index));
        }
        return result.toString();
    }

    public static String randomString(int length, StringChar stringChar) {
        if (stringChar == null)
            stringChar = StringChar.AlphaNumeric;
        return randomString(length, stringChar.getValue());
    }

    public static String randomAlphaNumericString(int length) {
        return randomString(length, StringChar.AlphaNumeric);
    }

    public static String randomNumericString(int length) {
        return randomString(length, StringChar.Numeric);
    }
}
