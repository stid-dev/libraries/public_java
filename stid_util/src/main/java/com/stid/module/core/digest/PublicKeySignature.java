package com.stid.module.core.digest;

import lombok.Getter;

import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.Signature;

@Getter
public class PublicKeySignature {
    private final PublicKey publicKey;
    private final HashAlgorithm hashAlgorithm;
    private final String encryptionAlgorithm;
    private final String provider;

    public PublicKeySignature(PublicKey publicKey, HashAlgorithm hashAlgorithm, String provider) {
        this.publicKey = publicKey;
        this.provider = provider;
        this.hashAlgorithm = hashAlgorithm;
        if (publicKey.getAlgorithm().startsWith("EC")) {
            this.encryptionAlgorithm = "ECDSA";
        } else {
            this.encryptionAlgorithm = publicKey.getAlgorithm();
        }
    }

    public boolean verify(byte[] data, byte[] signature) throws GeneralSecurityException {
        String verifyMode = hashAlgorithm + "with" + encryptionAlgorithm;
        Signature sig;
        if (provider == null)
            sig = Signature.getInstance(verifyMode);
        else
            sig = Signature.getInstance(verifyMode, provider);
        sig.initVerify(publicKey);
        sig.update(data);
        return sig.verify(signature);
    }
}
