package com.stid.module.core.otp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class SmartOTP {
    private static final Logger LOGGER = LoggerFactory.getLogger(SmartOTP.class);

    private final byte[] secret;
    private final Digits digit;
    private final OtpClock clock;
    private final int pastResponse;

    /***
     * SmartOTP interval = 30 and pastResponse = 1 and digit = SIX
     * @param secret secret key in base32
     */
    public SmartOTP(byte[] secret) {
        this.secret = secret;
        this.digit = Digits.SIX;
        this.clock = new OtpClock();
        pastResponse = 1;
    }

    public SmartOTP(byte[] secret, Digits digit) {
        this.secret = secret;
        this.digit = digit;
        this.clock = new OtpClock();
        pastResponse = 1;
    }

    public SmartOTP(byte[] secret, int interval, int pastResponse) {
        this.secret = secret;
        this.digit = Digits.SIX;
        this.clock = new OtpClock(interval);
        this.pastResponse = pastResponse;
    }

    public SmartOTP(byte[] secret, Digits digit, int interval, int pastResponse) {
        this.secret = secret;
        this.digit = digit;
        this.clock = new OtpClock(interval);
        this.pastResponse = pastResponse;
    }

    public static SmartOTP getInstance(byte[] secret) {
        return new SmartOTP(secret);
    }

    public static SmartOTP getInstance(byte[] secret, int interval, int pastResponse) {
        return new SmartOTP(secret, interval, pastResponse);
    }

    public String now() {
        return this.leftPadding(this.hash(this.secret, this.clock.getCurrentInterval()));
    }

    public boolean verify(String otp) {
        long code = Long.parseLong(otp);
        long currentInterval = this.clock.getCurrentInterval();
        int pastResponse = Math.max(this.pastResponse, 0);
        for (int i = pastResponse; i >= 0; --i) {
            int candidate = this.generate(this.secret, currentInterval - (long) i);
            if ((long) candidate == code) {
                LOGGER.debug("OTP '{}' is valid", otp);
                return true;
            }
        }
        LOGGER.debug("OTP '{}' is invalid", otp);
        return false;
    }

    private int generate(byte[] secret, long interval) {
        return this.hash(secret, interval);
    }

    private int bytesToInt(byte[] hash) {
        int offset = hash[hash.length - 1] & 15;
        int binary = (hash[offset] & 127) << 24 | (hash[offset + 1] & 255) << 16 | (hash[offset + 2] & 255) << 8 | hash[offset + 3] & 255;
        return binary % digit.getValue();
    }

    private String leftPadding(int otp) {
        if (digit == Digits.FOUR)
            return String.format("%04d", otp);
        if (digit == Digits.FIVE)
            return String.format("%05d", otp);
        if (digit == Digits.SIX)
            return String.format("%06d", otp);
        if (digit == Digits.SEVEN)
            return String.format("%07d", otp);
        if (digit == Digits.EIGHT)
            return String.format("%08d", otp);
        else
            return String.valueOf(otp);
    }

    private int hash(byte[] secret, long interval) {
        byte[] hash = new byte[0];

        try {
            hash = (new Hmac(Hash.SHA1, secret, interval)).digest();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return this.bytesToInt(hash);
    }

    public enum Digits {
        FOUR(10000),
        FIVE(100000),
        SIX(1000000),
        SEVEN(10000000),
        EIGHT(100000000);

        private final int digits;

        Digits(int digits) {
            this.digits = digits;
        }

        public int getValue() {
            return this.digits;
        }
    }

    private enum Hash {
        SHA1("HMACSHA1");

        private final String hash;

        Hash(String hash) {
            this.hash = hash;
        }

        public String toString() {
            return this.hash;
        }
    }

    private static class OtpClock {
        private final int interval;

        public OtpClock() {
            this.interval = 30;
        }

        public OtpClock(int interval) {
            this.interval = interval;
        }

        public long getCurrentInterval() {
            Calendar calendar = GregorianCalendar.getInstance(TimeZone.getTimeZone("UTC"));
            long currentTimeSeconds = calendar.getTimeInMillis() / 1000L;
            return currentTimeSeconds / (long) this.interval;
        }
    }

    private static class Hmac {
        private static final String ALGORITHM = "RAW";
        private final Hash hash;
        private final byte[] secret;
        private final long currentInterval;

        private Hmac(Hash hash, byte[] secret, long currentInterval) {
            this.hash = hash;
            this.secret = secret;
            this.currentInterval = currentInterval;
        }

        public byte[] digest() throws NoSuchAlgorithmException, InvalidKeyException {
            byte[] challenge = ByteBuffer.allocate(8).putLong(this.currentInterval).array();
            Mac mac = Mac.getInstance(this.hash.toString());
            SecretKeySpec macKey = new SecretKeySpec(this.secret, ALGORITHM);
            mac.init(macKey);
            return mac.doFinal(challenge);
        }
    }
}
