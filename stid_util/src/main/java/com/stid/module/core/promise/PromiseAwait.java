package com.stid.module.core.promise;

import java.util.concurrent.TimeoutException;

public class PromiseAwait<T> {
    private PromiseResult<T> result;
    private final Thread thread;
    private final IPromiseResolve<T> resolve = success -> this.result = new PromiseResult<>(success, null);
    private final IPromiseReject<Exception> reject = error -> this.result = new PromiseResult<>(null, error);

    public PromiseAwait(IPromiseHandler<T> handler) {
        thread = new Thread(() -> handler.callback(resolve, reject));
    }

    private PromiseResult<T> await(long timeoutMillis) {
        long sleep = timeoutMillis / 1000 < 10 ? timeoutMillis / 10 : 1000;
        long timeout = timeoutMillis;
        try {
            while (timeout > 0) {
                if (result != null) {
                    return result;
                }
                timeout -= sleep;
                Thread.sleep(sleep);
            }
            return new PromiseResult<>(null, new TimeoutException());
        } catch (Exception e) {
            return new PromiseResult<>(null, e);
        }
    }

    public PromiseResult<T> getResult(long timeout) {
        if (timeout <= 0 || timeout > 300000)
            timeout = 300000;

        this.result = null;
        this.thread.start();
        return await(timeout);
    }

    public interface IPromiseHandler<T> {
        void callback(IPromiseResolve<T> resolve, IPromiseReject<Exception> reject);
    }

    public interface IPromiseResolve<S> {
        void callback(S success);
    }

    public interface IPromiseReject<E> {
        void callback(E error);
    }
}
