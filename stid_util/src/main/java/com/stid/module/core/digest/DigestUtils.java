package com.stid.module.core.digest;

import com.stid.module.core.convert.Base64Converter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.IESParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public final class DigestUtils {
    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null)
            Security.addProvider(new BouncyCastleProvider());
    }

    public static byte[] generateHash(String algorithm, String message) throws NoSuchAlgorithmException {
        return generateHash(algorithm, message.getBytes());
    }

    public static byte[] generateHash(String algorithm, byte[] message) throws NoSuchAlgorithmException {
        return MessageDigest.getInstance(algorithm).digest(message);
    }

    public static byte[] generateHash(HashAlgorithm algorithm, String message) {
        return generateHash(algorithm, message.getBytes(StandardCharsets.UTF_8));
    }

    public static byte[] generateHash(HashAlgorithm algorithm, byte[] message) {
        try {
            return generateHash(algorithm.name(), message);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] encryptMessage(String message, String password) throws Exception {
        /* Encrypt the message. */
        Cipher cipherEncrypt = Cipher.getInstance("AES/ECB/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(generateHash(HashAlgorithm.SHA256, password), "AES");
        cipherEncrypt.init(Cipher.ENCRYPT_MODE, keySpec);
        return cipherEncrypt.doFinal(message.getBytes(StandardCharsets.UTF_8));
    }

    public static String decryptMessage(byte[] encryptedMessage, String password) throws Exception {
        /* Decrypt the message, given derived encContentValues and initialization vector. */
        Cipher cipherDecrypt = Cipher.getInstance("AES/ECB/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(generateHash(HashAlgorithm.SHA256, password), "AES");
        cipherDecrypt.init(Cipher.DECRYPT_MODE, keySpec);
        return new String(cipherDecrypt.doFinal(encryptedMessage), StandardCharsets.UTF_8);
    }

    public static byte[] encryptMessage(byte[] message, PublicKey publicKey) throws Exception {
        String keyAlg = publicKey.getAlgorithm();
        IESParameterSpec iesParamSpec = null;
        if (KeyAlgorithm.EC.name().equalsIgnoreCase(keyAlg)) {
            keyAlg = "ECIES";
            iesParamSpec = new IESParameterSpec(null, null, 128, 128, null);
        }

        Cipher cipherDecrypt = Cipher.getInstance(keyAlg, new BouncyCastleProvider());
        cipherDecrypt.init(Cipher.ENCRYPT_MODE, publicKey, iesParamSpec);
        return cipherDecrypt.doFinal(message);
    }

    public static byte[] decryptMessage(byte[] encryptedData, PrivateKey privateKey) throws Exception {
        String keyAlg = privateKey.getAlgorithm();
        IESParameterSpec iesParamSpec = null;
        if (KeyAlgorithm.EC.name().equalsIgnoreCase(keyAlg)) {
            keyAlg = "ECIES";
            iesParamSpec = new IESParameterSpec(null, null, 128, 128, null);
        }

        Cipher cipherDecrypt = Cipher.getInstance(keyAlg, BouncyCastleProvider.PROVIDER_NAME);
        cipherDecrypt.init(Cipher.DECRYPT_MODE, privateKey, iesParamSpec);
        return cipherDecrypt.doFinal(encryptedData);
    }

    public static String encryptData(byte[] data, byte[] publicKeyBytes, KeyAlgorithm keyAlgorithm) {
        try {
            PublicKey publicKey = getPublicKey(publicKeyBytes, keyAlgorithm);
            if (publicKey == null)
                throw new Exception("InvalidKey");
            byte[] bytes = encryptMessage(data, publicKey);
            return Base64Converter.encodeToString(bytes);
        } catch (Exception e) {
            return null;
        }
    }

    public static String decryptData(byte[] data, byte[] privateKeyBytes, KeyAlgorithm keyAlgorithm) {
        try {
            PrivateKey privateKey = getPrivateKey(privateKeyBytes, keyAlgorithm);
            if (privateKey == null)
                throw new Exception("InvalidKey");
            byte[] bytes = decryptMessage(data, privateKey);
            return Base64Converter.encodeToString(bytes);
        } catch (Exception e) {
            return null;
        }
    }

    public static PrivateKey getPrivateKey(byte[] privateKeyBytes, String keyAlgorithm) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(keyAlgorithm);
        return keyFactory.generatePrivate(keySpec);
    }

    public static PublicKey getPublicKey(byte[] publicKeyBytes, String keyAlgorithm) throws NoSuchAlgorithmException, InvalidKeySpecException {
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(keyAlgorithm);
        return keyFactory.generatePublic(keySpec);
    }

    public static PublicKey getPublicKey(byte[] publicKeyBytes, KeyAlgorithm keyAlgorithm) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return getPublicKey(publicKeyBytes, keyAlgorithm.name());
    }

    public static PrivateKey getPrivateKey(byte[] privateKeyBytes, KeyAlgorithm keyAlgorithm) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return getPrivateKey(privateKeyBytes, keyAlgorithm.name());
    }

    public static boolean verifySignature(byte[] data, byte[] signature, HashAlgorithm hashAlgorithm, PublicKey publicKey) {
        try {
            PublicKeySignature publicKeySignature = new PublicKeySignature(publicKey, hashAlgorithm, null);
            return publicKeySignature.verify(data, signature);
        } catch (Exception e) {
            return false;
        }
    }
}
