package com.stid.module.core.generate;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
    public static long nanoTime(Instant instant) {
        if (instant == null)
            instant = Instant.now();
        String timeString = String.valueOf(instant.getEpochSecond()) + instant.getNano();
        return Long.parseLong(timeString);
    }

    public static long nanoTime() {
        return nanoTime(null);
    }

    public static Instant startOfHour(Instant instant, int nextHours) {
        return startOfHour(instant.toEpochMilli(), nextHours);
    }

    public static Instant startOfDay(Instant instant) {
        return startOfDay(instant.toEpochMilli());
    }
    public static Instant startOfDay(long now) {
        return startOfDay(now, 0);
    }

    public static Instant endOfDay(Instant instant) {
        return endOfDay(instant.toEpochMilli());
    }
    public static Instant endOfDay(long now) {
        return endOfDay(now, 0);
    }

    public static Instant endOfDay(long now, int nextDays) {
        Instant startOfNextDay = startOfDay(now, nextDays + 1);
        return startOfNextDay.plusNanos(-1);
    }

    public static Instant startOfHour(long now, int nextHours) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(now));
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.add(Calendar.HOUR, nextHours);
        return cal.getTime().toInstant();
    }

    public static Instant startOfDay(long now, int nextDays) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(now));

        cal.add(Calendar.DAY_OF_MONTH, nextDays);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime().toInstant();
    }

    public static Instant startOfWeek(long now, int nextWeeks) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(now));

        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.WEEK_OF_YEAR, nextWeeks);
        return cal.getTime().toInstant();
    }

    public static Instant startOfMonth(long now, int nextMonths) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(now));

        cal.set(Calendar.DATE, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.MONTH, nextMonths);
        return cal.getTime().toInstant();
    }

    public static Instant startOfYear(long now, int nextYears) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(now));

        cal.set(Calendar.DAY_OF_YEAR, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.YEAR, nextYears);
        return cal.getTime().toInstant();
    }

    public static String formatTimeNow(String format) {
        return formatTime(System.currentTimeMillis(), format);
    }

    public static String formatTime(long milli, String format) {
        try {
            return new SimpleDateFormat(format).format(new Date(milli));
        } catch (Exception e) {
            return Long.toString(milli);
        }
    }
}
