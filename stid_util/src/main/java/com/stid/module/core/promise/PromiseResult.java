package com.stid.module.core.promise;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class PromiseResult<T> {
    private final T success;
    private final Exception error;

    PromiseResult(T success, Exception error) {
        this.success = success;
        this.error = error;
    }

    public boolean isSuccess() {
        return error == null;
    }
}
