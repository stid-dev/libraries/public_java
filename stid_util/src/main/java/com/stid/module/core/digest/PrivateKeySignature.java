package com.stid.module.core.digest;

import lombok.Getter;

import java.security.*;

@Getter
public class PrivateKeySignature {
    private final PrivateKey privateKey;
    private final HashAlgorithm hashAlgorithm;
    private final String provider;
    private final String encryptionAlgorithm;

    public PrivateKeySignature(PrivateKey privateKey, HashAlgorithm hashAlgorithm, String provider) {
        this.privateKey = privateKey;
        this.hashAlgorithm = hashAlgorithm;
        this.provider = provider;
        if (privateKey.getAlgorithm().startsWith("EC")) {
            this.encryptionAlgorithm = "ECDSA";
        } else {
            this.encryptionAlgorithm = privateKey.getAlgorithm();
        }
    }

    public byte[] sign(byte[] b) throws GeneralSecurityException {
        String signMode = hashAlgorithm + "with" + encryptionAlgorithm;
        Signature sig;
        if (provider == null)
            sig = Signature.getInstance(signMode);
        else
            sig = Signature.getInstance(signMode, provider);
        sig.initSign(privateKey);
        sig.update(b);
        return sig.sign();
    }
}
