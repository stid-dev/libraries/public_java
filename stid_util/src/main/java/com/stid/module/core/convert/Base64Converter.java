package com.stid.module.core.convert;

import java.util.Base64;

public class Base64Converter {

    public static byte[] encode(byte[] src) {
        try {
            return Base64.getEncoder().encode(src);
        } catch (Exception e) {
            return null;
        }
    }

    public static String encodeToString(byte[] src) {
        try {
            return Base64.getEncoder().encodeToString(src);
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] decode(byte[] src) {
        try {
            return Base64.getDecoder().decode(src);
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] decodeFromString(String src) {
        try {
            return Base64.getDecoder().decode(src);
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] encodeUrlSafe(byte[] src) {
        try {
            return Base64.getUrlEncoder().encode(src);
        } catch (Exception e) {
            return null;
        }
    }

    public static String encodeToUrlSafeString(byte[] src) {
        try {
            return Base64.getUrlEncoder().encodeToString(src);
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] decodeUrlSafe(byte[] src) {
        try {
            return Base64.getUrlDecoder().decode(src);
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] decodeFromUrlSafeString(String src) {
        try {
            return Base64.getUrlDecoder().decode(src);
        } catch (Exception e) {
            return null;
        }
    }
}
