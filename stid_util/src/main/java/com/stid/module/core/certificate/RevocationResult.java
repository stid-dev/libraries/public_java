package com.stid.module.core.certificate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@ToString
@AllArgsConstructor
public class RevocationResult implements Serializable {
    private final RevocationStatus status;
    private final String message;

    public RevocationResult(RevocationStatus status) {
        this.status = status;
        this.message = null;
    }
}
