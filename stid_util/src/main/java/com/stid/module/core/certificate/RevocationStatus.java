package com.stid.module.core.certificate;

public enum RevocationStatus {
    UNKNOWN, REVOKED, GOOD;
}
