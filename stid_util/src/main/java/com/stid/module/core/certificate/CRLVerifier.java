package com.stid.module.core.certificate;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.URL;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CRLVerifier {
    private static final Logger LOGGER = LoggerFactory.getLogger(CRLVerifier.class);
    @Getter(AccessLevel.PRIVATE)
    private final HashMap<String, X509CRL> cache = new HashMap<>();

    private boolean useCache;

    public CRLVerifier(boolean useCache) {
        this.useCache = useCache;
    }

    public void clearCache() {
        cache.clear();
    }

    public List<String> getCrl(X509Certificate x509Certificate) throws Exception {
        List<String> crlUrls = new ArrayList<>();
        X509CertificateHolder holder;
        try {
            holder = new X509CertificateHolder(x509Certificate.getEncoded());
        } catch (Exception e) {
            throw new Exception("Cannot read certificate to get CRL urls", e);
        }

        final CRLDistPoint crlDistPoint = CRLDistPoint.fromExtensions(holder.getExtensions());
        if (crlDistPoint == null) {
            throw new Exception("Certificate doesn't have CRL Distribution Points");
        }

        for (DistributionPoint point : crlDistPoint.getDistributionPoints()) {
            DistributionPointName pointName = point.getDistributionPoint();
            if (pointName != null && pointName.getType() == DistributionPointName.FULL_NAME) {
                GeneralNames genNames = GeneralNames.getInstance(pointName.getName());
                if (genNames != null) {
                    for (GeneralName genName : genNames.getNames()) {
                        if (genName.getTagNo() == GeneralName.uniformResourceIdentifier) {
                            String url = DERIA5String.fromByteArray(genName.getName().toASN1Primitive().getEncoded()).toString();
                            crlUrls.add(url);
                        }
                    }
                }
            }
        }
        return crlUrls;
    }

    public X509CRL downloadCrl(String crlURL) {
        InputStream crlStream = null;
        try {
            URL url = new URL(crlURL);
            crlStream = url.openStream();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            return (X509CRL) cf.generateCRL(crlStream);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {
            if (crlStream != null) {
                try {
                    crlStream.close();
                } catch (Exception ignored) {
                }
            }
        }
        return null;
    }

    public RevocationStatus checkStatus(X509Certificate x509Certificate) throws Exception {
        List<String> list = getCrl(x509Certificate);
        for (String crlUrl : list) {
            if (isUseCache()) {
                X509CRL x509CRL = cache.get(crlUrl);
                if (x509CRL != null && x509CRL.getNextUpdate().after(new Date())) {
                    return getStatus(x509CRL, x509Certificate);
                }
            }
            X509CRL x509CRL = downloadCrl(crlUrl);
            if (x509CRL != null) {
                if (isUseCache())
                    cache.put(crlUrl, x509CRL);
                return getStatus(x509CRL, x509Certificate);
            }
        }
        return RevocationStatus.UNKNOWN;
    }

    private RevocationStatus getStatus(X509CRL x509CRL, X509Certificate peerCert) {
        if (x509CRL.isRevoked(peerCert)) {
            return RevocationStatus.REVOKED;
        } else {
            return RevocationStatus.GOOD;
        }
    }

}
