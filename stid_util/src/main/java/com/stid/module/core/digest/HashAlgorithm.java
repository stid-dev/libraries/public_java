package com.stid.module.core.digest;

import lombok.Getter;

@Getter
public enum HashAlgorithm {
    MD5("1.2.840.113549.2.5"),
    SHA1("1.3.14.3.2.26"),
    SHA256("2.16.840.1.101.3.4.2.1");

    private final String oid;

    HashAlgorithm(String oid) {
        this.oid = oid;
    }

    public static HashAlgorithm parse(String algorithm, HashAlgorithm defaultValue) {
        try {
            return HashAlgorithm.valueOf(algorithm);
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
