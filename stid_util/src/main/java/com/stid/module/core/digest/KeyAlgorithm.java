package com.stid.module.core.digest;

public enum KeyAlgorithm {
    RSA, EC;

    public static KeyAlgorithm parse(String keyAlgorithm, KeyAlgorithm defaultValue) {
        try {
            return KeyAlgorithm.valueOf(keyAlgorithm);
        } catch (Exception e) {
            return defaultValue;
        }
    }
}
